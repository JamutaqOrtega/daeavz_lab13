﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Infraestructure;
using System.Data.Entity;

namespace Service
{
    public class ProductService
    {
        public List<Product> Get()
        {
            using (var context = new Context())
            {
                return context.Products.Where(x => x.EstaActivo == true).ToList();
            }
        }

        public Product GetById(int id)
        {
            using (var context = new Context())
            {
                return context.Products.Find(id);
            }
        }

        public void Insert(Product product)
        {
            using (var context = new Context())
            {
                product.EstaActivo = true;
                product.Igv = product.PrecioVenta * 0.18;
                context.Products.Add(product);
                context.SaveChanges();
            }
        }

        public void Update(Product producto, int id)
        {
            using (var context = new Context())
            {
                var productoNew = context.Products.Find(id);

                productoNew.Nombre = string.IsNullOrEmpty(producto.Nombre) ? productoNew.Nombre : producto.Nombre;
                productoNew.Descripcion = string.IsNullOrEmpty(producto.Descripcion) ? productoNew.Descripcion : producto.Descripcion;
                productoNew.PrecioVenta = producto.PrecioVenta > 0 ? producto.PrecioVenta : productoNew.PrecioVenta;
                productoNew.Igv = producto.PrecioVenta > 0 ? producto.PrecioVenta * 0.18 : productoNew.Igv;
                context.SaveChanges();
            }
        }

        public void DeleteLogic(int id)
        {
            using (var context = new Context())
            {
                var producto = context.Products.Find(id);

                context.Entry(producto).State = EntityState.Modified;
                producto.EstaActivo = false;
                context.Products.Where(x => x.EstaActivo == false).ToList();
                context.SaveChanges();
            }
        }
    }
}
