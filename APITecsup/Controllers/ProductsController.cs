﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using APITecsup.Models.Request;
using APITecsup.Models.Response;
using Service;

namespace APITecsup.Controllers
{
    public class ProductsController : ApiController
    {
        // GET: Products
        [HttpGet]
        public List<ProductResponse> List()
        {
            var service = new ProductService();
            var productos = service.Get();

            //Convert Domaint to Response
            var response = productos.Select(x => new ProductResponse
            {
                ProductID = x.ProductID,
                Nombre = x.Nombre,
                Descripcion = x.Descripcion,
                PrecioVenta = x.PrecioVenta,
                FechaCreacion = x.FechaCreacion,
                FechaVencimiento = x.FechaVencimiento,
                EstaActivo = x.EstaActivo,
                Igv = x.Igv
            }).ToList();

            return response;
        }

        // GET: By ID
        [HttpGet]
        public IHttpActionResult GetProdById(int id)
        {
            var service = new ProductService();
            var prod = service.GetById(id);

            return Ok(prod);
        }

        // POST
        [HttpPost]
        public bool Create(ProductRequest request)
        {
            var response = true;
            try
            {
                var service = new ProductService();
                service.Insert(new Domain.Product
                {
                    Nombre = request.Nombre,
                    Descripcion = request.Descripcion,
                    FechaCreacion = request.FechaCreacion,
                    FechaVencimiento = request.FechaVencimiento,
                    PrecioVenta = request.PrecioVenta
                });
            }
            catch (Exception)
            {
                response = false;
            }

            return response;
        }

        // PUT
        [HttpPut]
        public bool Actualizar(int id, ProductRequest request)
        {
            var response = true;
            try
            {
                var service = new ProductService();
                service.Update(new Domain.Product
                {
                    Nombre = request.Nombre,
                    Descripcion = request.Descripcion,
                    PrecioVenta = request.PrecioVenta
                }, id);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                response = false;
            }

            return response;
        }

        // DELETE: 
        public IHttpActionResult DeleteProduct(int id)
        {
            var response = true;
            try
            {
                var service = new ProductService();
                service.DeleteLogic(id);
            }
            catch (Exception)
            {
                response = false;
            }

            return Ok(response);
        }
    }
}